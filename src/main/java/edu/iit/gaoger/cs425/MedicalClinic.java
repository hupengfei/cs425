package edu.iit.gaoger.cs425;

import java.sql.*;

public class MedicalClinic {
    public static void main(String[] args) throws SQLException {

        Connection conn = DBConnection.getInstance();
        Statement st = conn.createStatement();
        ResultSet rs = st.executeQuery("SELECT VERSION()");

        if (rs.next()) {
            System.out.println(rs.getString(1));
        }
    }


}
