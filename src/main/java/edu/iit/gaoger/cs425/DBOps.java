package edu.iit.gaoger.cs425;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DBOps {

    private static final Logger logger = Logger.getLogger(DBOps.class.getName());

    public static void createTable(String sql) throws SQLException {
        Connection connection = DBConnection.getInstance();
        Statement statement = connection.createStatement();
        statement.executeUpdate(sql);
        statement.close();
        connection.close();
    }

    public static ResultSet query(String sql) throws SQLException {
        Connection connection = DBConnection.getInstance();
        Statement statement = connection.createStatement();
        ResultSet rs = statement.executeQuery(sql);
        return rs;
    }

    public static boolean userExist(String userid, String password) throws SQLException {
        String sql = "SELECT user_id, password FROM user_tbl where user_id ='" + userid + "';";
        ResultSet rs = query(sql);
        if (!rs.next()){
            logger.log(Level.SEVERE, "User does not exist");
            return false;
        } else if (!rs.getString("password").equals(password)) {
            logger.log(Level.SEVERE, "Userid and password does not match");
            return false;
        } else {
            return true;
        }
    }
}
