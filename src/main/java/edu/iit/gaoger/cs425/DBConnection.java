package edu.iit.gaoger.cs425;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class DBConnection {

    private static Connection connection;

    public static Connection getInstance(){
        if (connection == null)
            connect();
        return connection;
    }

    private static void connect() {
        final String driver = "org.postgresql.Driver";
        final String url = "jdbc:postgresql://localhost:5432/medical";
        final String user = "gaoger";
        final String password = "Admin!23";
        try {
            Class.forName(driver);
            connection = DriverManager.getConnection(url, user, password);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
